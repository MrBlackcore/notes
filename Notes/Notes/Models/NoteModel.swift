import UIKit

class NoteModel {
    var uuid:String?
    var text:String?
    var noteView:UIView?
    var blueColorView:UIView?
    var pinkColorView:UIView?
    var yellowColorView:UIView?
    var deleteView:UIView?
    var scaleView:UIView?
    var textView:UITextView?
}
