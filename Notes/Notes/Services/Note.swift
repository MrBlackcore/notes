import UIKit
import FirebaseDatabase

class Note {
    
    private var currentNoteModel:NoteModel?
    var noteModels:[NoteModel] = []
    var currentNoteId:String = ""
    
    var previousNoteViewPoint:CGPoint?
    lazy var noteItemSizeAndWidth:CGFloat = 30
    
    let dataBase = Database.database().reference()
    let viewController:UIViewController
    private let noteWidth:CGFloat
    private let noteHeight:CGFloat
    private let scaleIconName = "scaleIcon"
    private let deleteIconName = "deleteIcon"
    
    init(viewController: UIViewController) {
        self.viewController = viewController
        self.noteWidth = viewController.view.frame.size.width/2
        self.noteHeight = viewController.view.frame.size.width/2
    }
    
    //MARK: - NoteView
    
    func addNote(point: CGPoint,isFresh: Bool)  {
        let noteModel = NoteModel()
        currentNoteModel = noteModel
        let noteView = UIView()
        noteView.backgroundColor = .yellow
        noteView.frame = CGRect(x: point.x, y: point.y, width: noteWidth, height: noteHeight)
        noteView.layer.cornerRadius = noteItemSizeAndWidth/2
        noteView.clipsToBounds = true
        noteView.layer.borderWidth = 1
        noteView.layer.borderColor = UIColor.gray.cgColor
        currentNoteModel!.noteView = noteView
        noteModels.append(currentNoteModel!)
        noteView.tag = noteModels.count
        viewController.view.addSubview(noteView)
        createColorViews(on: noteView)
        addTextView(on: noteView)
        addScalingView(on: noteView)
        addDeleteView(on: noteView)
        addPanGestureRecogniser(on: noteView, option: .move)
        if isFresh {
            let uuid = UUID().uuidString
            noteModel.uuid = uuid
            currentNoteId = uuid
            createNoteJSON(for: noteView, with: uuid)
        }
        observeChanges(of: noteModel)
        observeDeletion()
    }
    
    private func observeChanges(of model: NoteModel) {
        Database.database().reference().child("notes").child(currentNoteId).observe(.childChanged) { (noteSnapshot) in
            let value = [noteSnapshot.key : noteSnapshot.value]
            self.updateView(model, key: self.currentNoteId, value: value as [String:Any])
        }
    }
    
    private func observeDeletion() {
        Database.database().reference().child("notes").observe(.childRemoved) { (noteSnapshot) in
            let key = noteSnapshot.key
            var index = 0
            for note in self.noteModels {
                if key == note.uuid {
                    note.noteView!.removeFromSuperview()
                    self.noteModels.remove(at: index)
                    self.currentNoteId = note.uuid!
                    self.dataBase.child("notes").child(note.uuid!).removeValue()
                    self.refreshModelTags(in: self.noteModels)
                }
                index += 1
            }
        }
    }
    
    func updateView(_ model: NoteModel, key: String, value: [String:Any]) {
        guard let noteView = model.noteView else {
            return
        }
        viewController.view.bringSubviewToFront(noteView)
        if let value = value["x"] as? CGFloat {
            noteView.frame.origin.x = viewController.view.frame.size.width*value
        }
        if let value = value["y"] as? CGFloat {
            noteView.frame.origin.y = viewController.view.frame.size.height*value
        }
        if let value = value["height"] as? CGFloat {
            noteView.frame.size.height = viewController.view.frame.size.height*value
        }
        if let value = value["width"] as? CGFloat {
            noteView.frame.size.width = viewController.view.frame.size.width*value
        }
        if let value = value["color"] as? String {
            setColor(color: value, model: model)
        }
        if let value = value["fontSize"] as? CGFloat {
            model.textView!.font = .systemFont(ofSize: value)
        }
        if let value = value["text"] as? String {
            model.textView?.text = value
        }
    }
    
    private func addPanGestureRecogniser(on view: UIView, option: PanGestureRecognizer) {
        var panGestureRecognizer = UIPanGestureRecognizer()
        switch option {
        case .move:
            panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(moveView))
        case .scale:
            panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(resizeView))
        }
        view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc private func moveView(sender: UIPanGestureRecognizer) {
        guard let senderView = sender.view else {
            return
        }
        for note in noteModels {
            if note.noteView!.tag == senderView.tag {
                viewController.view.bringSubviewToFront(note.noteView!)
                let point = sender.location(in: viewController.view)
                note.noteView!.center = point
                let x = note.noteView!.frame.origin.x/viewController.view.frame.size.width
                let y = note.noteView!.frame.origin.y/viewController.view.frame.size.height
                currentNoteId = note.uuid!
                dataBase.child("notes").child(note.uuid!).updateChildValues(["x": x, "y": y])
            }
        }
    }
        
    //MARK: - TextView
    
    private func addTextView(on view: UIView) {
        let textView = UITextView()
        textView.allowsEditingTextAttributes = true
        textView.text = ""
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(textView)
        textView.addDoneButtonOnKeyboard()
        textView.tag = noteModels.count
        textView.delegate = viewController as? UITextViewDelegate
        currentNoteModel!.textView = textView
        NSLayoutConstraint(item: textView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: noteItemSizeAndWidth).isActive = true
        NSLayoutConstraint(item: textView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: noteItemSizeAndWidth).isActive = true
    }
    
    //MARK: - ColorViews
    
    private func createColorViews(on view: UIView) {
        let blueColorView = UIView()
        let pinkColorView = UIView()
        let yellowColorView = UIView()
        configColorView(blueColorView, color: .blue, on: view)
        configColorView(pinkColorView, color: .pink, on: view)
        configColorView(yellowColorView, color: .yellow, on: view)
    }
    
    private func configColorView(_ view: UIView, color: Color, on note: UIView) {
        switch color {
        case .blue:
            view.backgroundColor = .systemBlue
            view.frame = CGRect(x: note.bounds.origin.x, y: note.bounds.origin.y, width: noteItemSizeAndWidth, height: noteItemSizeAndWidth)
            view.tag = 1
            currentNoteModel!.blueColorView = view
        case .pink:
            view.backgroundColor = .systemPink
            view.frame = CGRect(x: note.bounds.origin.x + noteItemSizeAndWidth, y: note.bounds.origin.y, width: noteItemSizeAndWidth, height: noteItemSizeAndWidth)
            view.tag = 2
            currentNoteModel!.pinkColorView = view
        case .yellow:
            view.backgroundColor = .systemYellow
            view.frame = CGRect(x: note.bounds.origin.x + noteItemSizeAndWidth*2, y: note.bounds.origin.y, width: noteItemSizeAndWidth, height: noteItemSizeAndWidth)
            view.tag = 3
            currentNoteModel!.yellowColorView = view
        }
        view.layer.cornerRadius = noteItemSizeAndWidth/2
        addTapOnColorView(view, mainView: note)
        note.addSubview(view)
        view.alpha = 0
    }
    
    private func addTapOnColorView(_ view: UIView, mainView: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(changeNoteColor))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func changeNoteColor(sender: UITapGestureRecognizer) {
        guard let colorView = sender.view else {
            return
        }
        for note in noteModels {
            if colorView.isDescendant(of: note.noteView!) {
                var color = "yellow"
                switch colorView.tag {
                case 1:
                    note.noteView!.backgroundColor = .blue
                    color = "blue"
                case 2:
                    note.noteView!.backgroundColor = UIColor(red: 255/255.0, green: 113/255.0, blue: 101/255.0, alpha: 1)
                    color = "pink"
                case 3:
                    note.noteView!.backgroundColor = .yellow
                    color = "yellow"
                default:
                    color = "yellow"
                }
                currentNoteId = note.uuid!
                dataBase.child("notes").child(note.uuid!).updateChildValues(["color": color])
            }
        }
    }
    
    //MARK: - ScalingView
    
    private func addScalingView(on view: UIView) {
        let scalingView = UIView()
        scalingView.backgroundColor = .clear
        scalingView.translatesAutoresizingMaskIntoConstraints = false
        scalingView.layer.cornerRadius = noteItemSizeAndWidth/2
        view.addSubview(scalingView)
        NSLayoutConstraint(item: scalingView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: scalingView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: scalingView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: noteItemSizeAndWidth).isActive = true
        NSLayoutConstraint(item: scalingView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: noteItemSizeAndWidth).isActive = true
        currentNoteModel!.scaleView = scalingView
        addImageView(on: scalingView,iconName: scaleIconName)
        addPanGestureRecogniser(on: scalingView,option: .scale)
    }
    
    private func addImageView(on view: UIView, iconName: String) {
        let imageView = UIImageView()
        imageView.frame = CGRect(x: 0, y: 0, width: noteItemSizeAndWidth, height: noteItemSizeAndWidth)
        imageView.image = UIImage(named: iconName)
        imageView.contentMode = .scaleToFill
        view.addSubview(imageView)
    }
    
    @objc private func resizeView(sender: UIPanGestureRecognizer) {
        guard let scalingView = sender.view else {
            return
        }
        for note in noteModels {
            if scalingView.isDescendant(of: note.noteView!) {
                let point = sender.location(in: note.noteView!)
                if point.x >= 0 && point.y >= 0 {
                    note.noteView!.frame = CGRect(x: note.noteView!.frame.origin.x, y: note.noteView!.frame.origin.y, width: point.x, height: point.y)
                    scalingView.frame.origin.x = point.x - noteItemSizeAndWidth
                    scalingView.frame.origin.y = point.y - noteItemSizeAndWidth
                    let width = note.noteView!.frame.size.width/viewController.view.frame.size.width
                    let height = note.noteView!.frame.size.height/viewController.view.frame.size.height
                    currentNoteId = note.uuid!
                    dataBase.child("notes").child(note.uuid!).updateChildValues(["width": width, "height": height])
                }
            }
        }
    }
    
    //MARK: - DeleteView
    
    private func addDeleteView(on view: UIView) {
        let deleteView = UIView()
        deleteView.translatesAutoresizingMaskIntoConstraints = false
        deleteView.layer.cornerRadius = noteItemSizeAndWidth/2
        deleteView.clipsToBounds = true
        view.addSubview(deleteView)
        deleteView.alpha = 0
        currentNoteModel!.deleteView = deleteView
        addTapOnDeleteView(deleteView)
        NSLayoutConstraint(item: deleteView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: deleteView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: deleteView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: noteItemSizeAndWidth).isActive = true
        NSLayoutConstraint(item: deleteView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: noteItemSizeAndWidth).isActive = true
        addImageView(on: deleteView, iconName: deleteIconName)
    }
    
    private func addTapOnDeleteView(_ view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(deleteNote))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func deleteNote(sender: UITapGestureRecognizer) {
        guard let deleteView = sender.view else {
            return
        }
        var index = 0
        for note in noteModels {
            if deleteView.isDescendant(of: note.noteView!) {
                note.noteView!.removeFromSuperview()
                noteModels.remove(at: index)
                currentNoteId = note.uuid!
                dataBase.child("notes").child(note.uuid!).removeValue()
                refreshModelTags(in: noteModels)
            }
            index += 1
        }
    }
    
    private func refreshModelTags(in modelsArray: [NoteModel]) {
        var tag = 1
        for model in modelsArray {
            model.noteView!.tag = tag
            model.textView?.tag = tag
            tag += 1
        }
    }
    
    func animateSettingsViews(tag: Int) {
        for model in noteModels {
            if model.textView?.tag == tag {
                animateView(model.blueColorView!)
                animateView(model.pinkColorView!)
                animateView(model.yellowColorView!)
                animateView(model.deleteView!)
            }
        }
    }
    
    private func animateView(_ view: UIView) {
        var alpha:CGFloat = 0
        if view.alpha == 0 {
            alpha = 1
        }
        UIView.animate(withDuration: 0.3) {
            view.alpha = alpha
        }
    }
    
    func saveFontSize(tag: Int, size: Int) {
        for note in noteModels {
            if note.textView?.tag == tag {
                currentNoteId = note.uuid!
                dataBase.child("notes").child(note.uuid!).updateChildValues(["fontSize": size])
            }
        }
    }
    
    private func createNoteJSON(for view: UIView,with uuid: String) {
        let note:[String:Any] = [
            "x": view.frame.origin.x/viewController.view.frame.size.width as NSObject,
            "y": view.frame.origin.y/viewController.view.frame.size.height as NSObject,
            "width": view.frame.size.width/viewController.view.frame.size.width as NSObject,
            "height": view.frame.size.height/viewController.view.frame.size.height as NSObject,
            "fontSize": 12 as NSObject,
            "text": "" as NSObject,
            "color": "yellow"
        ]
        if noteModels.count == 0 {
            let notesDictionary = [String:Any]()
            dataBase.child("notes").setValue(notesDictionary)
            dataBase.child("notes").child(uuid).setValue(note)
        } else {
            dataBase.child("notes").child(uuid).setValue(note)
        }
    }
    
    func addNoteOnSuperView(with receivedDictionary: [String:Any], key: String) {
        currentNoteId = key
        var point = CGPoint(x: 0, y: 0)
        if let x = receivedDictionary["x"] as? CGFloat {
            point.x = x
        }
        if let y = receivedDictionary["y"] as? CGFloat {
            point.y = y
        }
        let convertedY = viewController.view.frame.size.height*point.y
        let convertedX = viewController.view.frame.size.width*point.x
        let safePoint = CGPoint(x: convertedX, y: convertedY)
        addNote(point: safePoint,isFresh: false)
        let noteToCustomize = noteModels[noteModels.count-1]
        noteToCustomize.uuid = key
        noteToCustomize.noteView?.tag = noteModels.count
        noteToCustomize.textView?.tag = noteModels.count
        let noteColor = receivedDictionary["color"] as? String
        setColor(color: noteColor!, model: noteToCustomize)
        var size = CGSize(width: noteWidth/viewController.view.frame.size.width, height: noteHeight/viewController.view.frame.size.height)
        if let height = receivedDictionary["height"] as? CGFloat {
            size.height = height
        }
        if let width = receivedDictionary["width"] as? CGFloat {
            size.width = width
        }
        let convertedHeight = viewController.view.frame.size.height*size.height
        let convertedWidth = viewController.view.frame.size.width*size.width
        noteToCustomize.noteView?.frame.size = CGSize(width: convertedWidth, height: convertedHeight)
        if let text = receivedDictionary["text"] as? String, !text.isEmpty {
            noteToCustomize.textView?.text = text
        } else {
            noteToCustomize.textView?.text = ""
        }
        if let fontSize = receivedDictionary["fontSize"] as? CGFloat {
            noteToCustomize.textView!.font = .systemFont(ofSize: CGFloat(fontSize))
        } else {
            noteToCustomize.textView!.font = .systemFont(ofSize: CGFloat(12))
        }
    }
    
    private func setColor(color: String, model: NoteModel) {
        switch color {
        case "blue":
            model.noteView!.backgroundColor = .blue
        case "pink":
            model.noteView!.backgroundColor = UIColor(red: 255/255.0, green: 113/255.0, blue: 101/255.0, alpha: 1)
        case "yellow":
            model.noteView!.backgroundColor = .yellow
        default:
            model.noteView!.backgroundColor = .yellow
        }
    }
    
}
