import UIKit

protocol NoteDelegate {
    
    func moveNoteToVisiblePart(textViewTag: Int)
    func moveNoteBack(textViewTag: Int)
    func updateTextOnline(textViewTag: Int)
    
}
