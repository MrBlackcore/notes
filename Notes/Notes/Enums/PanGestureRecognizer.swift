import UIKit

enum PanGestureRecognizer {
    case move
    case scale
}
