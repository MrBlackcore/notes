import UIKit

extension NotesViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        currentTextView = textView
        var pointSize:CGFloat = 12
        if let size = textView.font?.pointSize {
            pointSize = size
        }
        fontSizeSlider.value = Float(pointSize)
        showFontSizeMenu()
        fontSizeLabel.text = "Font size: \(Int(pointSize))"
        note.moveNoteToVisiblePart(textViewTag: textView.tag)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        note.updateTextOnline(textViewTag: textView.tag)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        hideFontSizeMenu()
        note.moveNoteBack(textViewTag: textView.tag)
    }
    
}
