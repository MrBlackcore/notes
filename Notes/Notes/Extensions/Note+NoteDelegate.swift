import UIKit

extension Note: NoteDelegate {
    
    func moveNoteToVisiblePart(textViewTag: Int) {
        for note in noteModels {
            if note.noteView?.tag == textViewTag {
                animateSettingsViews(tag: textViewTag)
                guard let noteView = note.noteView else {
                    return
                }
                viewController.view.bringSubviewToFront(noteView)
                let point = CGPoint(x: noteView.frame.origin.x, y: noteView.frame.origin.y)
                previousNoteViewPoint = point
                UIView.animate(withDuration: 0.3) {
                    noteView.frame.origin.x = (self.viewController.view.frame.size.width - noteView.frame.size.width)/2
                    noteView.frame.origin.y = 100
                }
                saveTextIn(note)
            }
        }
    }
    
    func moveNoteBack(textViewTag: Int) {
        for note in noteModels {
            if note.noteView?.tag == textViewTag {
                animateSettingsViews(tag: textViewTag)
                guard let noteView = note.noteView, let previousNoteViewPoint = previousNoteViewPoint else {
                    return
                }
                UIView.animate(withDuration: 0.3) {
                    noteView.frame.origin.x = previousNoteViewPoint.x
                    noteView.frame.origin.y = previousNoteViewPoint.y
                }
                saveTextIn(note)
            }
        }
    }
    
    func updateTextOnline(textViewTag: Int) {
        for note in noteModels {
            if note.noteView?.tag == textViewTag {
                currentNoteId = note.uuid!
                if let text = note.textView?.text, !text.isEmpty {
                    note.text = text
                    dataBase.child("notes").child(note.uuid!).updateChildValues(["text": text])
                } else {
                    note.text = ""
                    dataBase.child("notes").child(note.uuid!).updateChildValues(["text": ""])
                }
            }
        }
    }
    
    private func saveTextIn(_ model: NoteModel) {
        currentNoteId = model.uuid!
        if let text = model.textView?.text, !text.isEmpty {
            model.text = text
            dataBase.child("notes").child(model.uuid!).updateChildValues(["text": text,
                                                                         "x":  model.noteView!.frame.origin.x/viewController.view.frame.size.width,
                                                                         "y":  model.noteView!.frame.origin.y/viewController.view.frame.size.height])
        } else {
            model.text = ""
            dataBase.child("notes").child(model.uuid!).updateChildValues(["text": "",
                                                                         "x":  model.noteView!.frame.origin.x/viewController.view.frame.size.width,
                                                                         "y":  model.noteView!.frame.origin.y/viewController.view.frame.size.height])
        }
    }
    
}
