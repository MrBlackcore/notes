import UIKit
import FirebaseDatabase

class NotesViewController: UIViewController {
    
    lazy var note = Note(viewController: self)
    
    var currentTextView:UITextView?
    
    var noteDelegate:NoteDelegate?
    
    let fontSizeView = UIView()
    let fontSizeLabel = UILabel()
    let fontSizeSlider = UISlider()
    
    let menuHeight:CGFloat = 120
    
    //MARK: - Main Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        noteDelegate = note
        addNoteWithTap()
        createFontSizeMenu()
        loadData()
    }
    
    //MARK: - Senders
    
    @objc private func findLocationForNote(touch: UITapGestureRecognizer) {
        let touchPoint = touch.location(in: self.view)
        let point = CGPoint(x: touchPoint.x, y: touchPoint.y)
        note.addNote(point: point, isFresh: true)
    }
    
    @objc private func changeFontSize(sender: UISlider) {
        fontSizeLabel.text = "Font size: " + String(Int(sender.value))
        guard let textView = currentTextView else {
            return
        }
        textView.font = .systemFont(ofSize: CGFloat(sender.value))
        note.saveFontSize(tag: textView.tag, size: Int(sender.value))
    }
    
    //MARK: - Flw Funtions
    
    private func addNoteWithTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(findLocationForNote))
        self.view.addGestureRecognizer(tap)
    }
    
    private func createFontSizeMenu() {
        fontSizeView.backgroundColor = .red
        fontSizeView.frame = CGRect(x: 0, y: -menuHeight, width: self.view.frame.size.width, height: menuHeight)
        fontSizeView.layer.cornerRadius = 5
        self.view.addSubview(fontSizeView)
        addLabel(on: fontSizeView)
        addSlider(on: fontSizeView)
    }

    private func addLabel(on view: UIView) {
        fontSizeLabel.frame = CGRect(x: 0, y: view.frame.size.height/2, width: view.frame.size.width, height: 20)
        fontSizeLabel.textAlignment = .center
        view.addSubview(fontSizeLabel)
    }

    private func addSlider(on view: UIView) {
        fontSizeSlider.frame = CGRect(x: 0, y: view.frame.size.height*(2/3), width: view.frame.size.width, height: view.frame.size.height/3)
        fontSizeSlider.minimumValue = 10
        fontSizeSlider.maximumValue = 50
        fontSizeSlider.tintColor = .green
        fontSizeSlider.isContinuous = true
        fontSizeSlider.isUserInteractionEnabled = true
        fontSizeSlider.addTarget(self, action: #selector(changeFontSize(sender:)), for: .valueChanged)
        view.addSubview(fontSizeSlider)
    }
    
    func showFontSizeMenu() {
        self.view.bringSubviewToFront(fontSizeView)
        changeMenuHeight(height: -self.menuHeight/3)
    }
    
    func hideFontSizeMenu() {
        changeMenuHeight(height: -self.menuHeight)
    }
    
    private func changeMenuHeight(height: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            self.fontSizeView.frame.origin.y = height
        }
    }
    
    private func loadData() {
        Database.database().reference().child("notes").observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                let key = snap.key
                Database.database().reference().child("notes").child(key).observeSingleEvent(of: .value) { (noteSnapshot) in
                    var noteDictionary = [String:Any]()
                    for note in noteSnapshot.children {
                        let noteSnap = note as! DataSnapshot
                        let value = noteSnap.value!
                        let noteSnapName = noteSnap.key
                        noteDictionary[noteSnapName] = value
                    }
                    self.note.addNoteOnSuperView(with: noteDictionary, key: key)
                }
            }
            self.observeNewNote()
        }
    }
    
    private func observeNewNote() {
        Database.database().reference().child("notes").observe(.childAdded) { (snapshot) in
            var isAdded = false
            for note in self.note.noteModels {
                if snapshot.key == note.uuid {
                    isAdded = true
                }
            }
            if !isAdded {
                let key = snapshot.key
                let value = snapshot.value as! [String:Any]
                self.note.addNoteOnSuperView(with: value, key: key)
            }
        }
    }
    
}
